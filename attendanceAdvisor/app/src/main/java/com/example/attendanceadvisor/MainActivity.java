package com.example.attendanceadvisor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
                                                                                            
    TextView txtView;
    EditText userName;
    Button btnLogar;
    ProgressBar progressBar;

    final String[] token = new String[1];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //new Mensagens();
        txtView = findViewById(R.id.txtBox);
        userName = findViewById(R.id.userName);
        btnLogar = findViewById(R.id.btnLogar);
        progressBar = findViewById(R.id.progressBar);

        progressBar.setVisibility(View.INVISIBLE);

        btnLogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!userName.getText().toString().isEmpty()) {
                    progressBar.setVisibility(View.VISIBLE);
                    FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            token[0] = task.getResult().getToken();
                            FirebaseFirestore db = FirebaseFirestore.getInstance();
                            Map<String, Object> tokenMap = new HashMap<>();
                            tokenMap.put("user", userName.getText().toString());
                            tokenMap.put("token", token[0]);
                            db.collection("tokens").document(userName.getText().toString()).set(tokenMap);
                            progressBar.setVisibility(View.INVISIBLE);
                        }

                    });
                }
            }
        });
    }
}
