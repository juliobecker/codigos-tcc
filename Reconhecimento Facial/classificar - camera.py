from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import Normalizer
from sklearn.svm import SVC
from keras.models import load_model
import os
import cv2
from PIL import Image
import numpy as np
from mtcnn.mtcnn import MTCNN
from pyfcm import FCMNotification
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import psycopg

con = psycopg2.connect(host='localhost', database='tcc',
user='postgres', password='tcc')
print("Banco de dados conectado!")

cred = credentials.Certificate("./serviceAccountKey.json")
firebase_admin.initialize_app(cred)

db = firestore.client()

users_ref = db.collection(u'tokens')
docs = users_ref.stream()

users = []
tokens = []
for doc in docs:
    users.append(doc.get('user'))
    tokens.append(doc.get('token'))
print("Tokens carregados do Firebase: ", users)

# extrai todas as faces de um arquivo de imagem,
# retornando a lista de faces, a imagem original e a posições das faces encontradas
def extract_face(frame, required_size=(160, 160)):
    # carrega a imagem do arquivo
    image = frame
    imageTrue = frame
    # conversão para RGB, se necessário
    #image = image.convert('RGB')
    # conversão para array
    pixels = np.asarray(image)
    # cria uma instância do detector, usando os pesos padrões
    detector = MTCNN()
    # detecta as faces na imagem
    results = detector.detect_faces(pixels)
    # extrai os quatro pontos delimitadores do retângulo em torno da primeira face
    faces = []      # lista para inserir as faces detectadas
    posicoes = []   # lista das posições das faces detectadas
    for i in range(len(results)):
            x1, y1, width, height = results[i]['box']
            # ajuste de índice de pixels negativos
            x1, y1 = abs(x1), abs(y1)
            x2, y2 = x1 + width, y1 + height
            # extrai a face da imagem original
            face = pixels[y1:y2, x1:x2]
            # redimensionamento dos pixels para o tamanho do modelo
            image = Image.fromarray(face)
            image = image.resize(required_size)
            face_array = np.asarray(image)
            faces.append(face_array)

            #cv2.imshow('image', imageTrue)
            # desenha um retângulo em volta da face detectada na imagem original
            cv2.rectangle(imageTrue, (x1,y1), (x2,y2), (255,0,0),2) 
            posicoes.append([x1,y1])

    return faces, imageTrue, posicoes

def get_embedding(model, face):
    # valores dos pixels
    face = face.astype('float32')
    # normalização
    mean, std = face.mean(), face.std()
    face = (face-mean)/std
    # transfere a face para uma amostra (3 dimensões para 4 dimensões)
    sample = np.expand_dims(face, axis=0)
    # faz a previsão dos pesos associados a face
    yhat = model.predict(sample)
    return yhat[0]

def notificar(token, titulo, mensagem):
    push_service = FCMNotification(api_key="AAAAz1un9zs:APA91bGWx52Jv-uOU-46FVsPwZIXIlTnkYvoFliN3_mSEYJNzZuTvy_iuATajXTIPLW79j02GMUMj6n_CkCzMzoNU3Hug_t3iU37sCLSD5UAGHXHCgNSIKR4xM4VkIjRalVXYOinGj7f")
    registration_id = token
    message_title = titulo
    message_body = mensagem
    result = push_service.notify_single_device(registration_id=registration_id, message_title=message_title, message_body=message_body)
    print("offline");
    
def inserirPresenca(aluno, status):
    cur = con.cursor()
    sql = "insert into public.\"Attendances\" (\"Date\", \"hourArrive\", \"hourDeparture\", present, \"createdAt\", \"updatedAt\", \"StudentId\", \"perCentPresent\") values ('" + str(date.today()) + "','" + str(date.today()) + "','" + str(date.today()) + "', true, '" + str(date.today()) + "','" + str(date.today()) + "', (SELECT id FROM public.\"Students\" WHERE public.\"Students\".\"Name\" = '" + aluno + "' )," + str(status) + ")"
    #print(sql)
    cur.execute(sql)
    con.commit()
    print("Frequência inserida no banco: %s - %s" % (aluno, status))

    
# normalização dos vetores de entrada
pesos = np.load('pesos.npz')
print('Pesos carregados')
emdTrainX = pesos['arr_0']
trainy = pesos['arr_1']
in_encoder = Normalizer()
emdTrainX_norm = in_encoder.transform(emdTrainX)
#emdTestX_norm = in_encoder.transform(emdTestX)
# codificação dos rótulos
out_encoder = LabelEncoder()
out_encoder.fit(trainy)
trainy_enc = out_encoder.transform(trainy)
#testy_enc = out_encoder.transform(testy)
# ajuste do modelo
model = SVC(kernel='linear', probability=True)
model.fit(emdTrainX_norm, trainy_enc)
# predict
#yhat_train = model.predict(emdTrainX_norm)
#yhat_test = model.predict(emdTestX_norm)
# score
#score_train = accuracy_score(trainy_enc, yhat_train)
#score_test = accuracy_score(testy_enc, yhat_test)
# summarize
#print('Accuracy: train=%.3f, test=%.3f' % (score_train*100, score_test*100))


#classificando uma foto qualquer
facenet_model = load_model('./input/facenet_keras.h5')
print("Modelo Facenet carregado")

frames = 400
frame_atual = 0

alunos_presentes = []
alunos_atrasados = []
alunos_fimAula = []
alunos_antecipada = []
alunos_falta = []
#lista_alunos = ["Eder de Almeida", "Guilherme Rodrigues", "Julio Becker"]
lista_alunos = users

while(frame_atual < frames):
    camera = cv2.VideoCapture('rtsp://admin:12345@10.0.0.102/onvif_1452874186_2058955446')
    ret, frame = camera.read()
    frame_atual+=1
    camera.release()
    frame = cv2.resize(frame, None, fx=.7, fy=.7)
    faces, frame, posicoes = extract_face(frame)
    i = 0
    for face_qualquer in faces:
        if face_qualquer is None:
            print("Nenhum aluno detectado")
            continue
        face_qualquer_emb = get_embedding(facenet_model, face_qualquer)

        amostras = np.expand_dims(face_qualquer_emb, axis=0)
        yhat_classe = model.predict(amostras)
        yhat_probabilidade = model.predict_proba(amostras)

        indice_classe = yhat_classe[0]
        probabilidade_classe = yhat_probabilidade[0, indice_classe] * 100
        nomes_previstos = out_encoder.inverse_transform(yhat_classe)
        todos_nomes = out_encoder.inverse_transform([0])

        if probabilidade_classe < 95:
            nomes_previstos[0] = "Nao identificado"
        else:
            if frame_atual <= frames * 0.1:
                if nomes_previstos[0] not in alunos_presentes:
                    alunos_presentes.append(nomes_previstos[0])
            elif frames * 0.1 < frame_atual <= frames * 0.5:
                if nomes_previstos[0] not in alunos_presentes and nomes_previstos[0] not in alunos_atrasados:
                    alunos_atrasados.append(nomes_previstos[0])
            elif frame_atual >= frames * 0.8:
                if nomes_previstos[0] not in alunos_fimAula and (nomes_previstos[0] in alunos_presentes or nomes_previstos[0] in alunos_atrasados):
                    alunos_fimAula.append(nomes_previstos[0])


        print("Aluno detectado: " + nomes_previstos[0])
            
        #plt.imshow(face_qualquer)
        #titulo = '%s (%.3f)' % (nomes_previstos[0], probabilidade_classe)
        #plt.title(titulo)
        #plt.show()
            
        cv2.putText(frame, nomes_previstos[0] + "- %.2f" % probabilidade_classe, (posicoes[i][0],posicoes[i][1]),cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,0,0),1, cv2.LINE_AA)
        i+=1
    if frame_atual == int(frames * 0.1):
        print("Alunos presentes:")
        print(alunos_presentes)
        for aluno in alunos_presentes:
            notificar(tokens[users.index(aluno)], "Status de frequência", "Aluno " + aluno + " está presente")
    elif frame_atual == int(frames * 0.5):
        print("Alunos atrasados:")
        print(alunos_atrasados)
        for aluno in alunos_atrasados:
            notificar(tokens[users.index(aluno)], "Status de frequência", "Aluno " + aluno + " chegou atrasado")
        for aluno in lista_alunos:
            if aluno not in alunos_presentes and aluno not in alunos_atrasados:
                alunos_falta.append(aluno)
        print("Alunos com falta:")
        print(alunos_falta)
        for aluno in alunos_falta:
            notificar(tokens[users.index(aluno)], "Status de frequência", "Aluno " + aluno + " faltou")
    elif frame_atual == frames:
        for aluno in alunos_presentes:
            if aluno not in alunos_fimAula:
                alunos_antecipada.append(aluno)
        for aluno in alunos_atrasados:
            if aluno not in alunos_fimAula:
                alunos_antecipada.append(aluno)
        print("Alunos com saída antecipada:")
        print(alunos_antecipada)
        for aluno in alunos_antecipada:
            notificar(tokens[users.index(aluno)], "Status de frequência", "Aluno " + aluno + " saiu antecipadamente")
    print('Frame ' + str(frame_atual) + '----------------------------------------')
    cv2.imshow('frame',frame)   
    if cv2.waitKey(10) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break
    
for aluno in alunos_presentes:
    if aluno in alunos_antecipada:
        inserirPresenca(aluno, 0.8)
    else:
        inserirPresenca(aluno, 1)

for aluno in alunos_atrasados:
    if aluno in alunos_antecipada:
        inserirPresenca(aluno, 0.3)
    else:
        inserirPresenca(aluno, 0.5)
        
for aluno in alunos_falta:
    inserirPresenca(aluno, 0)
