import os
from PIL import Image
import numpy as np
from mtcnn.mtcnn import MTCNN
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img

def extract_face(filename, required_size=(160, 160)):
    global i
    # carrega a imagem do arquivo
    image = Image.open(filename)
    #image = image.rotate(270)
    #plt.imshow(image)
    #plt.show()
    # conversão para RGB, caso necessário
    image = image.convert('RGB')
    # conversão para array
    pixels = np.asarray(image)
    # cria uma instância do detector, usando os pesos padrões
    detector = MTCNN()
    # detecta as faces na imagem
    results = detector.detect_faces(pixels)
    #print("Qtde de faces: " + str(len(results)))
    # extrai os quatro pontos delimitadores do retângulo em torno da primeira face
    x1, y1, width, height = results[0]['box']
    # ajuste de índice de pixels negativos
    x1, y1 = abs(x1), abs(y1)
    x2, y2 = x1 + width, y1 + height
    # extrai a face da imagem original
    face = pixels[y1:y2, x1:x2]
    # redimensionamento dos pixels para o tamanho do modelo
    image = Image.fromarray(face)
    image = image.resize(required_size)
    face_array = np.asarray(image)
    #if len(results) >= 1:
    #    plt.imshow(image)
    #    plt.show()
    #image.save('./detectadas/' + str(i) + '.jpg')
    #i += 1
    return face_array

def load_face(dir):
    for filename in os.listdir(dir):
        path = dir + filename
        face = extract_face(path)
        face = face.reshape((1,) + face.shape)
        qtdeGeradas = 1
        for batch in datagen.flow(face, batch_size=32,
                                      save_to_dir=dir, save_format='jpeg'):
            face_batch = extract_face
            qtdeGeradas += 1
            if qtdeGeradas > 5:
                break

def load_dataset(dir):        
    for subdir in os.listdir(dir):
        print("Aluno: " + subdir)
        path = dir + subdir + '/'
        faces = load_face(path)

datagen = ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.1,
        height_shift_range=0.1,
        zoom_range=[1, 5],
        horizontal_flip=True,
        fill_mode='constant')

load_dataset('./input/train/')
print("ACABOU")
