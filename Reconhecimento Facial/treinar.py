import numpy as np
from keras.models import load_model

# carrega so dados das faces extraídas
data = np.load('facesExtraidasKerasGen.npz')
trainX, trainy = data['arr_0'], data['arr_1']
print('Dados das faces carregados!')

# carrega o modelo pré-treinado do facenet
facenet_model = load_model('./input/facenet_keras.h5')
print('Modelo facenet carregado!')

def get_embedding(model, face):
    # valores dos pixels
    face = face.astype('float32')
    # normalização
    mean, std = face.mean(), face.std()
    face = (face-mean)/std
    # transfere a face para uma amostra (3 dimensões para 4 dimensões)
    sample = np.expand_dims(face, axis=0)
    # faz a previsão dos pesos associados a face
    yhat = model.predict(sample)
    return yhat[0]
   
# converte cada face do conjunto de treinamento em pesos
emdTrainX = list()
cont = 1
for face in trainX:
    emd = get_embedding(facenet_model, face)
    emdTrainX.append(emd)
    print("progresso: %.2f%%" % ((cont/len(trainX))*100), end="\r")
    cont+=1
    
emdTrainX = np.asarray(emdTrainX)
print(emdTrainX.shape)

# salva os valores de pesos em um arquivo em formato compactado
np.savez_compressed('pesos.npz', emdTrainX, trainy)
print('Pesos salvos.')
