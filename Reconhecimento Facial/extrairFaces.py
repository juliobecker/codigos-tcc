# Código para extração de faces a partir das fotos retiradas em sala

import numpy as np
import cv2                            # opencv
from mtcnn.mtcnn import MTCNN         # biblioteca que possui o método para extração de faces
#from matplotlib import pyplot as plt # Biblioteca para plotar imagens. Usada para ajudar no debug de extração de faces
from PIL import Image                 # Bibliteca para manipulação de imagens
import os                             # Biblioteca para manipular funcionalidades do sistema operacional

i = 1
cont = 1  


# extrai uma única face de uma arquivo de imagem fornecido
def extract_face(filename, required_size=(160, 160)):
    global i
    # carrega a imagem do arquivo
    image = Image.open(filename)
    #image = image.rotate(270)
    #plt.imshow(image)
    #plt.show()
    # conversão para RGB, caso necessário
    image = image.convert('RGB')
    # conversão para array
    pixels = np.asarray(image)
    # cria uma instância do detector, usando os pesos padrões
    detector = MTCNN()
    # detecta as faces na imagem
    results = detector.detect_faces(pixels)
    #print("Qtde de faces: " + str(len(results)))
    # extrai os quatro pontos delimitadores do retângulo em torno da primeira face
    x1, y1, width, height = results[0]['box']
    # ajuste de índice de pixels negativos
    x1, y1 = abs(x1), abs(y1)
    x2, y2 = x1 + width, y1 + height
    # extrai a face da imagem original
    face = pixels[y1:y2, x1:x2]
    # redimensionamento dos pixels para o tamanho do modelo
    image = Image.fromarray(face)
    image = image.resize(required_size)
    face_array = np.asarray(image)
    #if len(results) >= 1:
    #    plt.imshow(image)
    #    plt.show()
    image.save('./detectadas/' + str(i) + '.jpg')
    i += 1
    return face_array

def load_face(dir):
    global qtdeArquivos, cont
    faces = list()
    # enumeração dos arquivos
    for filename in os.listdir(dir):
        try:
            path = dir + filename
            face = extract_face(path)
            faces.append(face)
            print("Progresso: %.2f%% " % ((cont/qtdeArquivos) * 100),end="\r")
            cont+=1
            #print("Face detectada")
        except:
            print("Face não detectada")
    return faces

def load_dataset(dir):
    pastas = len(os.listdir(dir))            
    # lista para faces e seus respectivos rótulos (nome)
    X, y = list(), list()
    for subdir in os.listdir(dir):
        print("carregando " + subdir)
        path = dir + subdir + '/'
        faces = load_face(path)
        labels = [subdir for i in range(len(faces))]
        print("carregado %d amostras para a classe: %s" % (len(faces),subdir) ) # imprime o progresso
        X.extend(faces)
        y.extend(labels)
    return np.asarray(X), np.asarray(y)

qtdeArquivos = 0
for subdir in os.listdir('./input/train/'):
    qtdeArquivos += len(os.listdir('./input/train/' + subdir))

# carrega os dados das faces dentro da pasta 'train'
trainX, trainy = load_dataset('./input/train/')
print(trainX.shape, trainy.shape)
# load test dataset
#testX, testy = load_dataset('./input/val/')
#print(testX.shape, testy.shape)

# salva e comprime os dados das faces para uso posterior
np.savez_compressed('facesExtraidasKerasGen5imagens.npz', trainX, trainy)
print("Faces extraídas")
